package com.esgi.trademe.use_cases.user.application;

import com.esgi.trademe.kernel.CommandHandler;
import com.esgi.trademe.kernel.configurations.CodeException;
import com.esgi.trademe.kernel.exceptions.BadRequestException;
import com.esgi.trademe.use_cases.profile.domain.Profile;
import com.esgi.trademe.use_cases.user.domain.User;
import com.esgi.trademe.use_cases.user.domain.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public final class CreateUserCommandHandler implements CommandHandler<CreateUser, User> {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;

    @Autowired
    public CreateUserCommandHandler(UserRepository userRepository, PasswordEncoder passwordEncoder, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.modelMapper = modelMapper;
    }

    public User handle(CreateUser createUser) {
        LOGGER.info("Enregistrement du nouvel utilisateur en base de données : {}", createUser.getEmail());
        if(userRepository.findByEmail(createUser.getEmail()) == null){
            throw new BadRequestException(CodeException.ENTITY_ALREADY_EXISTING);
        }
        createUser.setPassword(passwordEncoder.encode(createUser.getPassword()));
        User user = modelMapper.map(createUser, User.class);
        user.setProfile(new Profile());
        return userRepository.save(user);
    }
}
