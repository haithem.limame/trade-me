package com.esgi.trademe.kernel.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ElementNotFoundException extends RuntimeException {

    public ElementNotFoundException(String message) {
        super(message);
    }

    public ElementNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
