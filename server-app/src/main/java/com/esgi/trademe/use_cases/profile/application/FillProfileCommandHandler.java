package com.esgi.trademe.use_cases.profile.application;

import com.esgi.trademe.kernel.CommandHandler;
import com.esgi.trademe.kernel.workflow.profile.StatutNouveau;
import com.esgi.trademe.use_cases.profile.domain.Profile;
import com.esgi.trademe.use_cases.profile.domain.ProfileRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public final class FillProfileCommandHandler implements CommandHandler<FillProfile, Profile> {
    private final ProfileRepository profileRepository;
    private final ModelMapper modelMapper;

    @Autowired
    public FillProfileCommandHandler(ProfileRepository profileRepository, ModelMapper modelMapper) {
        this.profileRepository = profileRepository;
        this.modelMapper = modelMapper;
    }

    public Profile handle(FillProfile fillProfile) {
        LOGGER.info("Charger le profile de l'utilisateur en base de données : {}", fillProfile.getId());
        Profile profile = modelMapper.map(fillProfile,Profile.class);
        profile.setStatut(new StatutNouveau());
        return profileRepository.save(profile);
    }
}
