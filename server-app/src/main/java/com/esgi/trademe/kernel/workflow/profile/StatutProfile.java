package com.esgi.trademe.kernel.workflow.profile;

import com.esgi.trademe.use_cases.profile.domain.Profile;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "nom")
@JsonSubTypes({
        @JsonSubTypes.Type(value = StatutNouveau.class, name = "StatutNouveau"),
        @JsonSubTypes.Type(value = StatutValide.class, name = "StatutValide"),
        @JsonSubTypes.Type(value = StatutValide.class, name = "StatutNonValide")
})
public interface StatutProfile {

    void next(Profile p);
    void prev(Profile p);
}
