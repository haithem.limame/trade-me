package com.esgi.trademe.use_cases.user.application;


import com.esgi.trademe.use_cases.user.domain.User;
import com.esgi.trademe.use_cases.user.domain.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Service used by Spring Security
 */
@Slf4j
@Service("userService")
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    public UserService(
            UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Méthode utilisée par Spring Security pour récupérer
     * les informations de l'utilisateur qui tente de se connecter
     * @param email de la personne qui essaie de se connecter
     * @return user dont l'email est celui fournit
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        if (email.trim().isEmpty()) {
            throw new UsernameNotFoundException("username is empty");
        }
        User user = userRepository.findByEmail(email);

        if (user == null) {
            LOGGER.error("User not found in the database");
            throw new UsernameNotFoundException("User " + email + " not found");
        } else {
            LOGGER.info("User found in the database : {}", email);
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().getDisplayName()));
        return authorities;
    }

}