package com.esgi.trademe.kernel.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = {RestController.class})
public class GlobalControllerExceptionHandler {

    public String getMessage(String messageCode) {
        return ResourceBundle.getBundle("messages-exceptions").getString(messageCode);
    }
    @ExceptionHandler(ElementNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ExceptionResponse handleDemandeNotFoundException(HttpServletResponse response, ElementNotFoundException exception) throws Exception {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                exception.getClass().getSimpleName(),
                HttpStatus.BAD_REQUEST.value(),
                getMessage(exception.getMessage()));

        LOGGER.error(exceptionResponse.getMessages().toString(), exceptionResponse.getExceptionType());
        return exceptionResponse;
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionResponse handleBadRequestException(HttpServletResponse response, BadRequestException exception) throws Exception {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                exception.getClass().getSimpleName(),
                HttpStatus.BAD_REQUEST.value(),
                getMessage(exception.getMessage()));

        LOGGER.error(exceptionResponse.getMessages().toString(), exceptionResponse.getExceptionType());
        return exceptionResponse;
    }

    @ExceptionHandler(ChangementStatutImpossibleException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionResponse handleChangementStatutImpossibleException(
            HttpServletResponse response,
            ChangementStatutImpossibleException exception) throws Exception {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                exception.getClass().getSimpleName(),
                HttpStatus.BAD_REQUEST.value(),
                getMessage(exception.getMessage()));
        LOGGER.error(exceptionResponse.getMessages().toString(), exceptionResponse.getExceptionType());
        return exceptionResponse;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionResponse handleMethodArgumentNotValidException (
            HttpServletResponse response,
            MethodArgumentNotValidException exception) throws Exception {
        Map<String, String> messagesErreurs = new HashMap<>();
        List<ObjectError> listeObjectError = exception.getAllErrors();
        listeObjectError.forEach( error -> {
            messagesErreurs.put(((FieldError)error).getField(), error.getDefaultMessage());
        });
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                exception.getClass().getSimpleName(),
                HttpStatus.BAD_REQUEST.value(),
                messagesErreurs);
        LOGGER.error(exceptionResponse.getMessages().toString(), exceptionResponse.getExceptionType());
        return exceptionResponse;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionResponse handleIllegalArgumentException(
            HttpServletResponse response,
            IllegalArgumentException exception) throws Exception {
        ExceptionResponse exceptionResponse = new ExceptionResponse(
                exception.getClass().getSimpleName(),
                HttpStatus.BAD_REQUEST.value(),
                exception.getMessage());
        LOGGER.error(exceptionResponse.getMessages().toString(), exceptionResponse.getExceptionType());
        return exceptionResponse;
    }

}
