package com.esgi.trademe.use_cases.profile.domain;

import com.esgi.trademe.kernel.workflow.profile.StatutProfile;
import com.esgi.trademe.use_cases.profession.domain.Profession;
import com.esgi.trademe.use_cases.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Profile")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Profile {
    @Id
    private String id;
    @DBRef
    private User worker;
    private Profession profession;
    private Double tjm;
    private String geographicalArea;
    private List<Skill> skills;
    private List<Certificate> certificates;
    private StatutProfile statut;

    public void nextState() {
        statut.next(this);
    }
    public void prevState() {
        statut.prev(this);
    }
}
