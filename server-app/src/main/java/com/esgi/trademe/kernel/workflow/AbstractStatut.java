package com.esgi.trademe.kernel.workflow;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

@Getter
@Setter
public abstract class AbstractStatut {

    @Transient
    protected String nom;
}
