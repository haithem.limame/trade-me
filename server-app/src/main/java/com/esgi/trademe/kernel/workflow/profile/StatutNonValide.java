package com.esgi.trademe.kernel.workflow.profile;

import com.esgi.trademe.kernel.configurations.CodeException;
import com.esgi.trademe.kernel.exceptions.ChangementStatutImpossibleException;
import com.esgi.trademe.kernel.workflow.AbstractStatut;
import com.esgi.trademe.use_cases.profile.domain.Profile;

public class StatutNonValide extends AbstractStatut implements StatutProfile {

    public StatutNonValide(){
        nom = this.getClass().getSimpleName();
    }

    @Override
    public void next(Profile p) {
        p.setStatut(new StatutValide());
    }

    @Override
    public void prev(Profile p) {
        throw new ChangementStatutImpossibleException(CodeException.CHANGEMENT_STATUT_IMPOSSIBLE);
    }




}