package com.esgi.trademe.use_cases.profession.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Metier")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Profession {
    private String id;
    private String name;
}
