package com.esgi.trademe.kernel.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse {

    private String exceptionType;
    private int code;
    private Map<String, String> messages;

    public ExceptionResponse(String exceptionType, int code, String message) {
        this.exceptionType = exceptionType;
        this.code = code;
        Map<String, String> messageTemp = new HashMap<>();
        messageTemp.put("message", message);
        this.messages = messageTemp;
    }

    @Override
    public String toString() {
        return "ExceptionResponse{" +
                "exceptionType='" + exceptionType + '\'' +
                ", code=" + code +
                ", messages='" + messages + '\'' +
                '}';
    }
}
