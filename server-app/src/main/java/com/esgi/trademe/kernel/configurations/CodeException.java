package com.esgi.trademe.kernel.configurations;

public class CodeException {

    public static String CHANGEMENT_STATUT_IMPOSSIBLE = "FUNC.3001";
    public static String ENTITY_NOT_FOUND = "FUNC.3002";
    public static String BAD_ID_UPDATE = "FUNC.3003";
    public static String ENTITY_ALREADY_EXISTING = "FUNC.3004";
    public static String UNAUTHORIZED_CLIENT = "FUNC.3005";

}
